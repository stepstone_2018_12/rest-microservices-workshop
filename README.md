# Rest/Microservices Workshop - StepStone 3-6.12.2018

### Jakich narzędzi potrzebujesz?

* git
* Java 8 (jdk)
* Dowolne IDE (Intellij, Eclipse)
* Postman, curl (lub dowolny klient HTTP)
* Docker: https://www.docker.com/products/docker-desktop
* (Opcjonalnie) Maven: https://maven.apache.org

